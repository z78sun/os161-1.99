#!/bin/bash

cd ~/cs350-os161/os161-1.99/kern/conf
./config ASST3
cd ../compile/ASST3
bmake depend
bmake
bmake install
cd ~/cs350-os161/os161-1.99
bmake
bmake install
#p uw-testbin/onefork
#p uw-testbin/pidcheck
#p uw-testbin/widefork
#p testbin/forktest
#p uw-testbin/argtesttest
#p testbin/sty