#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>
/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
//static struct semaphore *intersectionSem;

static struct lock* lk;

struct cv* cv_west;

struct cv* cv_east;

struct cv* cv_north;

struct cv* cv_south;

/*
const int local_max = 40;

int w_count = 0;
bool w_max = false;

int e_count = 0;
bool e_max = false;

int n_count = 0;
bool n_max = false;

int s_count = 0;
bool s_max = false;
*/
bool used;//true if the intersection is not empty
Direction cur;
int car = 0; 
int cars[] = {0,0,0,0};
/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void)
{
  /* replace this default implementation with your own implementation */
  cv_west = cv_create("west");
  cv_east = cv_create("east");
  cv_north = cv_create("north");
  cv_south = cv_create("south");
  lk = lock_create("inter");
  if (
    (cv_west == NULL) || 
    (cv_south == NULL) ||
    (cv_east == NULL) || 
    (cv_north == NULL) || 
    (lk == NULL) ) {
    panic("init failed");
  }
/*
  intersectionSem = sem_create("intersectionSem",1);
  if (intersectionSem == NULL) {
    panic("could not create intersection semaphore");
  }
*/
  return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  /* replace this default implementation with your own implementation */
  /*
  KASSERT(intersectionSem != NULL);
  sem_destroy(intersectionSem);
  */
 KASSERT(cv_west != NULL);
 KASSERT(cv_east != NULL);
 KASSERT(cv_north != NULL);
 KASSERT(cv_south != NULL);
 KASSERT(lk != NULL);
 cv_destroy(cv_west);
 cv_destroy(cv_east);
 cv_destroy(cv_south);
 cv_destroy(cv_north);
 lock_destroy(lk);
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination) 
{
  KASSERT(cv_west != NULL);
  KASSERT(cv_east != NULL);
  KASSERT(cv_north != NULL);
  KASSERT(cv_south != NULL);
  KASSERT(lk != NULL);
  (void) destination;
  lock_acquire(lk);
  if (origin == west) {
    cars[west] += 1;
    if ((used == false) || (cur == origin)) {
      used = true;
      car += cars[west];
      cars[west] = 0;
      cv_broadcast(cv_west, lk);
    } else {
      cv_wait(cv_west, lk);
    }
  } else if (origin == east) {
    cars[east] += 1;
    if ((used == false) || (cur == origin)) {
      used = true;
      car += cars[east];
      cars[east] = 0;
      cv_broadcast(cv_east, lk);
    } else {
      cv_wait(cv_east, lk);
    }
  } else if (origin == north) {
    cars[north] += 1;
    if ((used == false) || (cur == origin)) {
      used = true;
      car += cars[north];
      cars[north] = 0;
      cv_broadcast(cv_north, lk);
    } else {
      cv_wait(cv_north, lk);
    }
  } else if (origin == south) {
    cars[south] += 1;
    if ((used == false) || (cur == origin)) {
      used = true;
      car += cars[south];
      cars[south] = 0;
      cv_broadcast(cv_south, lk);
    } else {
      cv_wait(cv_south, lk);
    }
  }
  /*
  if (origin == west) {
    if (used == false || cur == origin) {
      if (w_max) {
        cv_wait(cv_west, lk);
        return;
      }
      cv_signal(cv_west, lk);
      w_count++;
      w_max = (w_count == local_max) ? true : false;
    } else {
      cv_wait(cv_west, lk);
    }
  } else if (origin == east) {
    if ((w_count == 0) && (n_count == 0) && (s_count == 0)) {
      if (e_max) {
        cv_wait(cv_east, lk);
        return;
      }
      cv_signal(cv_east, lk);
      e_count++;
      e_max = (e_count == local_max) ? true : false;
    } else {
      cv_wait(cv_east, lk);
    }
  } else if (origin == north) {
    if ((w_count == 0) && (e_count == 0) && (s_count == 0)) {
      if (n_max) {
        cv_wait(cv_north, lk);
        return;
      }
      cv_signal(cv_north, lk);
      n_count++;
      n_max = (n_count == local_max) ? true : false;
    } else {
      cv_wait(cv_north, lk);
    }
  } else if (origin == south) {
    if ((w_count == 0) && (e_count == 0) && (n_count == 0)) {
      if (s_max) {
        cv_wait(cv_south, lk);
        return;
      }
      cv_signal(cv_south, lk);
      s_count++;
      s_max = (s_count == local_max) ? true : false;
    } else {
      cv_wait(cv_south, lk);
    }
  }
  */
  lock_release(lk);

  /* 
  // replace this default implementation with your own implementation 
  (void)origin;  // avoid compiler complaint about unused parameter 
  (void)destination; // avoid compiler complaint about unused parameter 
  KASSERT(intersectionSem != NULL);
  P(intersectionSem);
  */

}


/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination) 
{
  KASSERT(cv_west != NULL);
  KASSERT(cv_east != NULL);
  KASSERT(cv_north != NULL);
  KASSERT(cv_south != NULL);
  KASSERT(lk != NULL);
  (void) origin;
  (void) destination;
  lock_acquire(lk);
  car -= 1;
  if (car == 0) {
    used = false;
  }
  /*
  if (origin == west) {
    w_count--;
    w_max = (w_count == 0) ? false : true;
  } else if (origin == east) {
    e_count--;
    e_max = (e_count == 0) ? false : true;
  } else if (origin == north) {
    n_count--;
    n_max = (n_count == 0) ? false : true;
  } else if (origin == south) {
    s_count--;
    s_max = (s_count == 0) ? false : true;
  } 
  */
  lock_release(lk);
  /*
  // replace this default implementation with your own implementation 
  (void)origin;  // avoid compiler complaint about unused parameter 
  (void)destination; // avoid compiler complaint about unused parameter 
  KASSERT(intersectionSem != NULL);
  V(intersectionSem);
  */
}
