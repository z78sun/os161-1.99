#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>
/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is is_empty as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
//static struct semaphore *intersectionSem;

static struct lock* lk;

static struct cv* cv_west;

static struct cv* cv_east;

static struct cv* cv_north;

static struct cv* cv_south;


volatile bool is_empty = true; //true if the intersection is empty
volatile Direction cur;
//volatile int car = 0; 
volatile int cars[] = {0,0,0,0};
/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void)
{
  /* replace this default implementation with your own implementation */
  cv_west = cv_create("west");
  cv_east = cv_create("east");
  cv_north = cv_create("north");
  cv_south = cv_create("south");
  lk = lock_create("inter");
  if (
    (cv_west == NULL) || 
    (cv_south == NULL) ||
    (cv_east == NULL) || 
    (cv_north == NULL) || 
    (lk == NULL) ) {
    panic("init failed");
  }
/*
  intersectionSem = sem_create("intersectionSem",1);
  if (intersectionSem == NULL) {
    panic("could not create intersection semaphore");
  }
*/
  return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  /* replace this default implementation with your own implementation */
  /*
  KASSERT(intersectionSem != NULL);
  sem_destroy(intersectionSem);
  */
 KASSERT(cv_west != NULL);
 KASSERT(cv_east != NULL);
 KASSERT(cv_north != NULL);
 KASSERT(cv_south != NULL);
 KASSERT(lk != NULL);
 cv_destroy(cv_west);
 cv_destroy(cv_east);
 cv_destroy(cv_south);
 cv_destroy(cv_north);
 lock_destroy(lk);
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination) 
{
  //KASSERT(cv_west != NULL);
  //KASSERT(cv_east != NULL);
  //KASSERT(cv_north != NULL);
  //KASSERT(cv_south != NULL);
  //KASSERT(lk != NULL);
  (void) destination;

  lock_acquire(lk);
  cars[origin] += 1;
  if (is_empty == true) {
    cur = origin;
    is_empty = false;
  }
  if (cur != origin) {
    if (origin == west) {
      cv_wait(cv_west, lk);
    } else if (origin == east) {
      cv_wait(cv_east, lk);
    } else if (origin == north) {
      cv_wait(cv_north, lk);
    } else if (origin == south) {
      cv_wait(cv_south, lk);
    }
  }
  lock_release(lk);

  /* 
  // replace this default implementation with your own implementation 
  (void)origin;  // avoid compiler complaint about unused parameter 
  (void)destination; // avoid compiler complaint about unused parameter 
  KASSERT(intersectionSem != NULL);
  P(intersectionSem);
  */

}


/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination) 
{
  //KASSERT(cv_west != NULL);
  //KASSERT(cv_east != NULL);
  //KASSERT(cv_north != NULL);
  //KASSERT(cv_south != NULL);
  //KASSERT(lk != NULL);
  //(void) origin;
  (void) destination;
  lock_acquire(lk);
  cars[origin] -= 1;
  if (cars[origin] == 0) {
    is_empty = true;
    int max = origin;
    for (int i = 0; i < 4; ++i) {
      if (cars[i] > cars[max]) {
        is_empty = false;
        max = i;
      }
    }  

    if (max == west) {
      cur = west;
      cv_broadcast(cv_west, lk);
    } else if (max == east) {
      cur = east;
      cv_broadcast(cv_east, lk);
    } else if (max == north) {
      cur = north;
      cv_broadcast(cv_north, lk);
    } else if (max == south) {
      cur = south;
      cv_broadcast(cv_south, lk);
    }
    
  }

  lock_release(lk);
  /*
  // replace this default implementation with your own implementation 
  (void)origin;  // avoid compiler complaint about unused parameter 
  (void)destination; // avoid compiler complaint about unused parameter 
  KASSERT(intersectionSem != NULL);
  V(intersectionSem);
  */
}
