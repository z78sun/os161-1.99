#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include "opt-A2.h"
#include <mips/trapframe.h>
#include <limits.h>
#include <vfs.h>
#include <kern/fcntl.h>

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

#if OPT_A2
extern struct lock* pid_lock;
extern struct cv* pid_cv;
static const int pid_size = PID_MAX - PID_MIN;
extern volatile struct pid_element* pidtable[PID_MAX - PID_MIN];
#endif //OPT_A2


#if OPT_A2
int sys_fork(struct trapframe *tf, pid_t *retval) {
  struct proc *child = proc_create_runprogram(curproc->p_name);
  KASSERT(child != NULL);
  child->p_cwd = curproc->p_cwd;
	child->console = curproc->console;

  (pidtable[child->pid - PID_MIN])->p = curproc;
  //address space
  spinlock_acquire(&child->p_lock);
  int err = as_copy(curproc_getas(), &(child->p_addrspace));
  if (err) panic("as copy err");
  spinlock_release(&child->p_lock);
  //trapframe
  struct trapframe* tf_copy = kmalloc(sizeof(struct trapframe));
  KASSERT(tf_copy != NULL);

  memcpy(tf_copy, tf, sizeof(struct trapframe));

  //thread fork
  thread_fork("child_thread", child, (void*)enter_forked_process, tf_copy, 0);
  *retval = child->pid;

	return(0);
}
#endif //OPT_A2

void sys__exit(int exitcode) {

  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */

  #if OPT_A2
    lock_acquire(pid_lock);
      int index = curproc->pid - PID_MIN;
      if ((pidtable[index])->p != NULL) {
        (pidtable[index])->exit_code = exitcode;
        (pidtable[index])->exit = true;
        cv_broadcast(pid_cv, pid_lock);
      }

    lock_release(pid_lock);
  #else 
    (void)exitcode;
  #endif //OPT_A2

  DEBUG(DB_SYSCALL,"Syscall: _exit(%d)\n",exitcode);

  KASSERT(curproc->p_addrspace != NULL);
  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);

  /* detach this thread from its process */
  /* note: curproc cannot be used after this call */
  proc_remthread(curthread);

  /* if this is the last user process in the system, proc_destroy()
     will wake up the kernel menu thread */
  proc_destroy(p);
  
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
}


/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
  #if OPT_A2
    *retval = curproc->pid;
  #else
  *retval = 1;
  #endif // OPT_A2 
  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
	    userptr_t status,
	    int options,
	    pid_t *retval)
{
  int exitstatus;
  int result;

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */

  if (options != 0) {
    return(EINVAL);
  }
  /* for now, just pretend the exitstatus is 0 */
  #if OPT_A2
    lock_acquire(pid_lock);
      int index = pid - PID_MIN;
      if ((pidtable[index])->p != curproc) {
	      lock_release(pid_lock);
	      return(ESRCH);
      }
      while ((pidtable[index])->exit == false) {
        cv_wait(pid_cv, pid_lock);
      }
      exitstatus = (pidtable[index])->exit_code;
    lock_release(pid_lock);
  #else
  exitstatus = 0;
  #endif //OPT_A2

  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    return(result);
  }
  *retval = pid;
  return(0);
}

#if OPT_A2
int sys_execv(const char * program_name, char ** args, int * retval)
{
	struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr;
	int result;
  (void)args;
  (void)retval;

  // copy over program name into kernel space
  size_t program_name_len = (strlen(program_name) + 1) * sizeof(char);
  char * program_name_kernel = kmalloc(program_name_len);
  KASSERT(program_name_kernel != NULL);
  int err = copyin((const_userptr_t) program_name, (void *) program_name_kernel, program_name_len);
  KASSERT(err == 0);
  /* kprintf(program_name_kernel); */


  // count number of args and copy into kernel
  // ...

  // copy program path into kernel
  // ...

	/* Open the file. */
	result = vfs_open(program_name_kernel, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* We should be a new process. */
	/* KASSERT(curproc_getas() == NULL); */

	/* Create a new address space. */
	as = as_create();
	if (as ==NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Switch to it and activate it. */
	curproc_setas(as);
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

	/* Warp to user mode. */
	enter_new_process(0 /*argc*/, NULL /*userspace addr of argv*/,
			  stackptr, entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
  return EINVAL;

}

#endif  // OPT_A2
